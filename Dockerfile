FROM openjdk:20
COPY target/tyneurekaserver-0.0.1-SNAPSHOT.jar tyneurekaserver-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/tyneurekaserver-0.0.1-SNAPSHOT.jar"]